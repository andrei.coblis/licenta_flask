from os import error
import joblib
from numpy import mean
from numpy import std
from sklearn.ensemble import BaggingClassifier
from sklearn import model_selection
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report, roc_auc_score
from sklearn.model_selection import cross_validate, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import precision_recall_fscore_support as score

df = pd.read_csv("E:\An4\Licenta\ML scripts\heart.csv")

categorical_val = []
continous_val = []
for column in df.columns:
    print('==============================')
    print(f"{column} : {df[column].unique()}")
    if len(df[column].unique()) <= 10:
        categorical_val.append(column)
    else:
        continous_val.append(column)

categorical_val.remove('target')
dataset = pd.get_dummies(df, columns=categorical_val)

s_sc = StandardScaler()
col_to_scale = ['age', 'trestbps', 'chol', 'thalach', 'oldpeak']
dataset[col_to_scale] = s_sc.fit_transform(dataset[col_to_scale])

X = dataset.drop('target', axis=1)
y = dataset.target

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.3, random_state=42)

model = BaggingClassifier(
    base_estimator=DecisionTreeClassifier(max_depth=1, max_features=1), n_estimators=1000, random_state=10)
model.fit(X_train, y_train)
y_score = model.predict(X_test)

precision, recall, fscore, support = score(y_test, y_score, average='binary')
roc = roc_auc_score(y_test, y_score)
accuracy = accuracy_score(y_test, y_score)

print('precision: {}'.format(precision))
print('recall: {}'.format(recall))
print('fscore: {}'.format(fscore))
print('support: {}'.format(support))
print('roc: {}'.format(roc))

performanceScores = [accuracy, precision, recall, fscore, roc]

print(performanceScores)

joblib.dump(model, 'Bagging Ensemble.joblib')
joblib.dump(performanceScores, 'baggingEnsemblePerformanceScores.pkl')

# scores = cross_val_score(model, X, y, scoring='accuracy',
#                          cv=kfold, n_jobs=-1, error_score='raise')

# score2 = cross_validate(estimator=model, X=X, y=y, return_train_score=True)
# print(score2)

# print('Accuracy: %.3f (%.3f)' % (mean(scores), std(scores)))

# model.fit(X_train, y_train)


def print_score(clf, X_train, y_train, X_test, y_test, train=True):
    if train:
        pred = clf.predict(X_train)
        clf_report = pd.DataFrame(classification_report(
            y_train, pred, output_dict=True))
        print("Train Result:\n================================================")
        print(f"Accuracy Score: {accuracy_score(y_train, pred) * 100:.2f}%")
        print("_______________________________________________")
        print(f"CLASSIFICATION REPORT:\n{clf_report}")
        print("_______________________________________________")
        print(f"Confusion Matrix: \n {confusion_matrix(y_train, pred)}\n")

    elif train == False:
        pred = clf.predict(X_test)
        clf_report = pd.DataFrame(
            classification_report(y_test, pred, output_dict=True))
        print("Test Result:\n================================================")
        print(f"Accuracy Score: {accuracy_score(y_test, pred) * 100:.2f}%")
        print("_______________________________________________")
        print(f"CLASSIFICATION REPORT:\n{clf_report}")
        print("_______________________________________________")
        print(f"Confusion Matrix: \n {confusion_matrix(y_test, pred)}\n")


# print_score(model, X_train, y_train, X_test, y_test, train=True)
# print_score(model, X_train, y_train, X_test, y_test, train=False)
