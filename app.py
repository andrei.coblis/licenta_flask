from os import error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score, roc_auc_score
from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.ensemble import BaggingClassifier
from sklearn.linear_model import LogisticRegression
from flask import Flask, config, json, request, jsonify, make_response
import joblib
import pandas as pd
import sys
from flask_mysqldb import MySQL
from flask_cors import CORS, cross_origin

app = Flask(__name__)

mysql = MySQL(app)
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ""
app.config['MYSQL_HOST'] = "127.0.0.1"
app.config['MYSQL_PORT'] = 3307
app.config['MYSQL_DB'] = "licenta"
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

logisticRegressionClassifier = joblib.load('classifier.joblib')
naiveBayesClassifier = joblib.load('naiveBayesClassifier.joblib')
baggingEnsembleClassifier = joblib.load('baggingEnsembleClassifier.joblib')

model_columns = joblib.load('model_columns.pkl')
logisticRegressionPerformanceScores = joblib.load(
    'logisticRegressionPerformanceScores.pkl')
naiveBayesPerformanceScores = joblib.load('naiveBayesPerformanceScores.pkl')
baggingEnsemblePerformanceScores = joblib.load(
    'baggingEnsemblePerformanceScores.pkl')


@app.route('/logisticRegression', methods=['GET', 'POST'])
@cross_origin("http://localhost:3000/")
def logisticRegressionPrediction():
    try:
        data = request.json
        query = pd.get_dummies(pd.DataFrame(data, columns=[
                               'age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg', 'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal'], index=[0]))
        query = query.reindex(columns=model_columns, fill_value=0)

        prediction = logisticRegressionClassifier.predict(query)
        results = {0: "Patient doesn't have heart disease",
                   1: "Patient has heart disease"}
        print(prediction, file=sys.stderr)
        response = jsonify({
            "statusCode": 200,
            "status": "Prediction made",
            "result": results[prediction[0]]
        })
        return response
    except Exception as error:
        return jsonify({
            "statusCode": 500,
            "status": "Could not make prediction",
            "error": str(error)
        })


@app.route('/makePrediction', methods=['GET', 'POST'])
@cross_origin("http://localhost:3000/")
def predict():
    try:
        data = request.json
        query = pd.get_dummies(pd.DataFrame(data, columns=[
            'age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg', 'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal'], index=[0]))
        query = query.reindex(columns=model_columns, fill_value=0)
        classifier = joblib.load(request.json['algorithm'] + '.joblib')
        prediction = classifier.predict(query)
        results = {0: "Patient doesn't have heart disease",
                   1: "Patient has heart disease"}
        response = jsonify({
            "statusCode": 200,
            "status": "Prediction made",
            "result": results[prediction[0]]
        })
        return response
    except Exception as error:
        return jsonify({
            "statusCode": 500,
            "status": "Could not make prediction",
            "error": str(error)
        })


@app.route('/naiveBayes', methods=['GET', 'POST'])
@cross_origin("http://localhost:3000/")
def naiveBayesPrediction():
    try:
        data = request.json
        print(data)
        query = pd.get_dummies(pd.DataFrame(data, columns=[
                               'age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg', 'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal'], index=[0]))
        print(query)
        print("__________________")
        query = query.reindex(columns=model_columns, fill_value=0)
        print(query)

        prediction = naiveBayesClassifier.predict(query)
        results = {0: "Patient doesn't have heart disease",
                   1: "Patient has heart disease"}
        print(prediction)
        response = jsonify({
            "statusCode": 200,
            "status": "Prediction made",
            "result": results[prediction[0]]
        })
        return response
    except Exception as error:
        return jsonify({
            "statusCode": 500,
            "status": "Could not make prediction",
            "error": str(error)
        })


@app.route('/trainAndSave', methods=['GET', 'POST'])
@cross_origin("http://localhost:3000/")
def trainAndSaveNewModel():
    df = pd.read_csv("E:\An4\Licenta\ML scripts\heart.csv")

    print(request.json, file=sys.stderr)

    categorical_val = []
    continous_val = []
    for column in df.columns:
        if len(df[column].unique()) <= 10:
            categorical_val.append(column)
        else:
            continous_val.append(column)

    categorical_val.remove('target')
    dataset = pd.get_dummies(df, columns=categorical_val)

    s_sc = StandardScaler()
    col_to_scale = ['age', 'trestbps', 'chol', 'thalach', 'oldpeak']
    dataset[col_to_scale] = s_sc.fit_transform(dataset[col_to_scale])

    X = dataset.drop('target', axis=1)
    y = dataset.target

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.3, random_state=42)

    parameters = request.json['parameters']
    model = LogisticRegression(
        penalty=parameters[0], solver=parameters[1], C=int(parameters[2]))

    if request.json['type'] == 'Logistic Regression':
        model.fit(X_train, y_train)

    if request.json['type'] == 'Bagging Ensemble':
        model = BaggingClassifier()
        model.fit(X_train, y_train)

    y_score = model.predict(X_test)
    precision, recall, fscore, support = score(
        y_test, y_score, average='binary')
    roc = roc_auc_score(y_test, y_score)
    accuracy = accuracy_score(y_test, y_score)

    performanceScores = [accuracy, precision, recall, fscore, roc]
    modelName = request.json['name']
    joblib.dump(model, modelName + '.joblib')
    joblib.dump(performanceScores, modelName + 'PerformanceScores.pkl')

    cursor = mysql.connection.cursor()
    cursor.execute('''SELECT MLAlgorithmId FROM mlalgorithms''')
    ids = cursor.fetchall()
    sql = "INSERT INTO mlalgorithms VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    val = (ids[-1], modelName, performanceScores[0], performanceScores[3], ','.join(
        parameters), performanceScores[1], performanceScores[4], performanceScores[2])
    cursor.execute(sql, val)
    print(val)
    mysql.connection.commit()

    return "Succes"


@app.route('/insertModelsInDb', methods=['GET', 'POST'])
@cross_origin("http://localhost:3000/")
def test():
    cursor = mysql.connection.cursor()
    sql = "INSERT INTO mlalgorithms VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    val = (3, 'Bagging Ensemble', baggingEnsemblePerformanceScores[0], baggingEnsemblePerformanceScores[3],
           '1000,1,10', baggingEnsemblePerformanceScores[1], baggingEnsemblePerformanceScores[4], baggingEnsemblePerformanceScores[2])
    cursor.execute(sql, val)
    mysql.connection.commit()

    return 'Success!'
